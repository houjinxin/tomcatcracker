批量IP扫描Tomcat后台管理页面,暴力密码破解

输入文件：
    项目根目录下的账号密码文件pwd.txt  
    每行格式：  “账号，密码”
输出文件：   
    项目根目录下的cracked.txt      
    每行格式：   "破解后的地址 （账号：密码）"

参数：
    prefiexIp：要扫描的ip地址的前面2位，如“121.36.”，程序会使用密码文件逐个扫描121.36.*.*的所有80和8080端口，找到后门漏洞
    maxThreadNum：并发线程数
 
注意：
    每个账号的密码不要超过5个，因为tomcat在同一账号5分钟内超过5次失败后会锁定该账号5分钟
 






启动类：com.freeman.bruteforceattact.TomcatAttacter
        run application
